<?php
namespace app\models;


use app\classes\DataParser;
use app\classes\Dater;
use app\classes\URLMaker;
use lithium\action\Controller;

class Eleves extends DataContainer {
    protected $type = "eleves_txt";
    protected $idParameter = "EV";
    protected static $instance;
    
    public static function getInstance(){
        if(is_null(self::$instance))
            self::$instance = new Eleves();
        return self::$instance;
    }
    
    protected function __construct()
    {
        parent::__construct($this->type);
    }


    public function getAll(){
        return $this->data;
    }
    
    public function getByPromoId($id){
        $data = [];
        foreach($this->data as $element) {
            if (isset($element['P0']) && $element['P0'] == $id)
                $data[] = $element;
        }
        return $data;
    }
    
    public function getById($id){
        foreach($this->data as $element) {
            if (isset($element[$this->idParameter]) && $element[$this->idParameter] == $id)
                return $element;
        }
        return false;
    }
    
    public function searchByName($input){
        $input = explode(" ", $input);

        return array_filter($this->data, function($data) use ($input) {
            $found = false;
            foreach($input as $element) {
                if (empty($element) || strlen($element) < 3 || $found) break; //Already found or not enough characters;
                $found = (stripos($data['NOM'], $element) !== false || stripos($data['PRENOM'], $element) !== false);
            }
            return $found;
        });
    }
}