<?php
namespace app\models;


use app\classes\DataParser;
use app\classes\Dater;
use app\classes\URLMaker;
use lithium\action\Controller;

class Salles extends DataContainer {
    protected $type = "salles_txt";
    protected $idParameter = "SA";
    protected static $instance;
    
    public static function getInstance(){
        if(is_null(self::$instance))
            self::$instance = new Salles();
        return self::$instance;
    }
    
    protected function __construct()
    {
        parent::__construct($this->type);
    }


    public function getAll(){
        return $this->data;
    }
    
    public function getById($id){
        foreach($this->data as $element) {
            if (isset($element[$this->idParameter]) && $element[$this->idParameter] == $id)
                return $element;
        }
        return false;
    }
}