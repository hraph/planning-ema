<?php
namespace app\models;


use app\classes\DataParser;
use app\classes\Dater;
use app\classes\URLMaker;
use lithium\action\Controller;

class Profs extends DataContainer {
    protected $type = "profs_txt";
    protected $idParameter = "PR";
    protected static $instance;
    
    public static function getInstance(){
        if(is_null(self::$instance))
            self::$instance = new Profs();
        return self::$instance;
    }
    
    protected function __construct()
    {
        parent::__construct($this->type);
    }


    public function getAll(){
        return $this->data;
    }
    
    public function getById($id){
        foreach($this->data as $element) {
            if (isset($element[$this->idParameter]) && $element[$this->idParameter] == $id)
                return $element;
        }
        return false;
    }
    
    public function searchByName($input){
        $input = explode(" ", $input);

        return array_filter($this->data, function($data) use ($input) {
            //var_dump($data);
            $found = false;
            foreach($input as $element) {
                if (empty($element) || strlen($element) < 3 || $found) break; //Already found or not enough characters;
                $found = (stripos($data['NOM'], $element) !== false || stripos($data['PRENOM'], $element) !== false);
            }
            return $found;
        });
    }
}