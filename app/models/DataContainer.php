<?php
namespace app\models;

use app\classes\URLMaker;
use app\classes\DataParser;

abstract class DataContainer {
    private $dataParser;
    protected $data = [];
    protected $type;
    protected $idParameter;

    abstract public static function getInstance();
    
    protected function __construct($type){
        $url = new URLMaker($type);
        $this->dataParser = new DataParser();
        $this->dataParser->getRawFromUrl($url->getURL(), true)->parse();
        $this->data = $this->dataParser->getParsed();
    }
    
    abstract public function getAll();
    
    abstract public function getById($id);
}