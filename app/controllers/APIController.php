<?php
namespace app\controllers;


use app\models\Eleves;
use app\models\Profs;
use app\models\Promos;
use app\models\Salles;
use lithium\action\Controller;
use app\classes\SelectorPromo;
use app\classes\SelectorProf;
use app\classes\SelectorEleve;
use app\classes\SelectorSalle;
use app\classes\URLMaker;
use app\classes\Dater;
use app\classes\DataParser;
use app\classes\ColorReader;

class APIController extends Controller{
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    public function getElevesByName(){
        if (isset($this->request->data['query'])) {
            $eleves = Eleves::getInstance()->searchByName($this->request->data['query']);
            
            $data = json_encode([
                "success" => ($eleves != false) ? true : false,
                "content" => $eleves
            ]);

            $this->render([
                'layout' => false,
                'template' => 'get',
                'data'  => [
                    'data' => $data
                ]
            ]);
            
        }
        else
            die();
    }
    
    public function getProfsByName(){
        if (isset($this->request->data['query'])) {
            $profs = Profs::getInstance()->searchByName($this->request->data['query']);
            
            $data = json_encode([
                "success" => ($profs != false) ? true : false,
                "content" => $profs
            ]);

            $this->render([
                'layout' => false,
                'template' => 'get',
                'data'  => [
                    'data' => $data
                ]
            ]);
            
        }
        else
            die();
    }
    
    public function getPlanningDate(){
        //Selector
        $urlMaker = new URLMaker('planning_txt');
        $urlMaker->setSelector(new SelectorPromo($this->request->params['id']));
    
        //Date begin
        $dater = (new Dater())->setTodayOrNextWorkingDay(); //today
        if (isset($this->request->params['date'])) //date given
            $dater->setStrDate($this->request->params['date']);

        if ($dater->checkValidTime()) {
            $promo = Promos::getInstance()->getById($this->request->params['id']);
            if ($promo) { //Valid id
                $urlMaker->setDate($dater->getDate());

                $dataParser = new DataParser();
                $dataParser->getRawFromUrl($urlMaker->getURL())->parse();
                $day = array_shift($dataParser->getParsed()); //Keep only first element of array
            
                $data = json_encode($day);
                $this->render([
                    'layout' => false,
                    'template' => 'get',
                    'data'  => [
                        'data' => $data
                    ]
                ]);
            }
            else {
                return $this->render([
                    'layout' => false,
                    'template' => 'get',
                    'data' => [
                        "error" => "Invalid parameter"    
                    ]
                ]);
            }
        }
        else {
            return $this->render([
                'layout' => false,
                'template' => 'get',
                'data' => [
                    "error" => "Invalid date"    
                ]
            ]);
        }
            
    }
    
    
    public function getPlannerDate(){
        //Selector
        $urlMaker = new URLMaker('planning_txt');
        
        //Get planning by type
        $object = null;
        switch ($this->request->params['planningType']) {
            case 'eleve':
                $urlMaker->setSelector(new SelectorEleve($this->request->params['id']));
                $object = Eleves::getInstance()->getById($this->request->params['id']);
                break;
            case 'prof':
                $urlMaker->setSelector(new SelectorProf($this->request->params['id']));
                $object = Profs::getInstance()->getById($this->request->params['id']);
                break;
            case 'salle':
                $urlMaker->setSelector(new SelectorSalle($this->request->params['id']));
                $object = Salles::getInstance()->getById($this->request->params['id']);
                break;
            case 'promo':
                $urlMaker->setSelector(new SelectorPromo($this->request->params['id']));
                $object = Promos::getInstance()->getById($this->request->params['id']);
                break;
            default:
                return $this->render([
                    'layout' => false,
                    'template' => 'get',
                    'data' => [
                        "error" => "Invalid type"    
                    ]
                ]);
                break;
        }
        
    
        //Date begin
        $daterBegin = (new Dater())->setTodayOrNextWorkingDay(); //today
        if (isset($this->request->params['date'])) //date given
            $daterBegin->setStrDate($this->request->params['date'])->setFirstDayOfWeek();
            
        $daterEnd = clone $daterBegin; //begin at dateBegin
        $daterEnd->setLastDayOfWeek();

        if ($daterBegin->checkValidTime()) {
            
            if ($object) { //Valid id
                $urlMaker->setDateBegin($daterBegin->getDate());
                $urlMaker->setDateEnd($daterEnd->getDate());

                $dataParser = new DataParser();
                $dataParser->getRawFromUrl($urlMaker->getURL())->parse();
                $days = $dataParser->groupByDays($dataParser->getParsed()); //Keep only first element of array
            
            
                //Create object for planner
                $planner = [];
                $planner["content"] = [];
                
                $customDaterPrev = clone $daterBegin;
                $customDaterNext = clone $daterBegin;
                $planner["prev"] = $customDaterPrev->setPrevWeek()->getDate();
                $planner["next"] = $customDaterNext->setNextWeek()->getDate();
                $colorReader = new ColorReader();
                
                foreach ($days as $key => $value) { //Foreach days
                    $planner["content"][$key] = []; //New planner day
                    $dayContent = $value["content"];
                    
                    //each events
                    foreach($dayContent as $element){
                        
                        if (isset($element["PL"])) { //Valid event key 
                            
                            $content = ($element['PROF'] != "-" && isset($element['PRCLE'])) ? $element['PROF'] : "";
                            $content .= ($element['SALLE'] != "-" && isset($element['SACLE'])) ? "\nSalle " . $element['SALLE'] : "";
                            $content .= ($element['GROUPE'] != "-") ? "\nGroupe ". $element['GROUPE'] : "";
                            $content .= (!empty($element['LANOTE'])) ? "\n(" . $element['LANOTE'] . ")" : "";
                            
                            $planner["content"][$key][$element["PL"]] = [
                                "id" => $element["PL"],
                                "title" => $element["COURS"],
                                "content" => $content,
                                "start" => date('D M d Y H:i:s O', strtotime($element["DATE"] . " " . $element["HD"])), //JS format
                                "end" => date('D M d Y H:i:s O', strtotime($element["DATE"] . " " . $element["HF"])),
                                "color" => ($element['CFOND'] != 0) ? $colorReader->setInt($element["CFOND"])->getHex() : "#ffffff"
                            ];
                        }
                    }
                }
                
                $data = json_encode($planner);

                $this->render([
                    'layout' => false,
                    'template' => 'get',
                    'data'  => [
                        'data' => $data
                    ]
                ]);
            }
            else {
                return $this->render([
                    'layout' => false,
                    'template' => 'get',
                    'data' => [
                        "error" => "Invalid parameter"    
                    ]
                ]);
            }
        }
        else {
            return $this->render([
                'layout' => false,
                'template' => 'get',
                'data' => [
                    "error" => "Invalid date"    
                ]
            ]);
        }
            
    }
    
    private function groupByDays($data){
        $return = [];
        foreach ($data as $dat){
            if (isset($dat['DATE'])) { //Has date
                $return[$dat['DATE']]['date'] = $dat['DATE']; //Save date
                $return[$dat['DATE']]['content'][] = $dat; //Save data
            }
        }
        return $return;
    }
}