<?php
namespace app\controllers;


use app\classes\DataParser;
use app\classes\Dater;
use app\classes\SelectorPromo;
use app\classes\SelectorSalle;
use app\classes\SelectorEleve;
use app\classes\SelectorProf;
use app\classes\URLMaker;
use app\models\Promos;
use app\models\Eleves;
use app\models\Salles;
use app\models\Profs;
use lithium\action\Controller;

class PlanningController extends Controller{
    private $urlMaker;
    
    public function __construct(array $config){
        parent::__construct($config);
        
        if (isset($this->request->params['id'])) //id given in URL
            $this->urlMaker = new URLMaker('planning_txt');
        else
            die();
    }

    public function day(){
        //Get planning by type
        $object = null;
        $relatedObject = null;
        switch ($this->request->params['planningType']) {
            case 'eleve':
                $this->urlMaker->setSelector(new SelectorEleve($this->request->params['id']));
                $object = Eleves::getInstance()->getById($this->request->params['id']);
                if ($object)
                    $relatedObject = Promos::getInstance()->getById($object['P0']);
                break;
            case 'prof':
                $this->urlMaker->setSelector(new SelectorProf($this->request->params['id']));
                $object = Profs::getInstance()->getById($this->request->params['id']);
                break;
            case 'salle':
                $this->urlMaker->setSelector(new SelectorSalle($this->request->params['id']));
                $object = Salles::getInstance()->getById($this->request->params['id']);
                break;
            case 'promo':
                $this->urlMaker->setSelector(new SelectorPromo($this->request->params['id']));
                $object = Promos::getInstance()->getById($this->request->params['id']);
                break;
            default:
                return $this->render([
                    'status' => 404,
                    'head' => true
                ]);
                break;
        }

        //Date begin
        $dater = (new Dater())->setTodayOrNextWorkingDay(); //today
        if (isset($this->request->params['date'])) //date given
            $dater->setStrDate($this->request->params['date']);

        if ($dater->checkValidTime()) {
            if ($object) { //Valid id
                $this->urlMaker->setDate($dater->getDate());

                $dataParser = new DataParser();
                $dataParser->getRawFromUrl($this->urlMaker->getURL())->parse();
                $days = $dataParser->groupByDays($dataParser->getParsed());
                $day = array_shift($days); //Keep only first element of array
    
                $title = (isset($object['NOM'])) ? $object['NOM'] : null;

                $this->render([
                    'data'  => [
                        'title' => $title,
                        'planningType' => $this->request->params['planningType'],
                        'itemId' => $this->request->params['id'],
                        'item' => $object,
                        'relatedItem' => $relatedObject,
                        'date' => $dater->getDate(),
                        'day' => $day,
                        'next' => $dater->cloneDater()->setNextDay()->getDate(),
                        'prev' => $dater->cloneDater()->setPrevDay()->getDate()
                    ]
                ]);
            }
            else
                return $this->render([
                    'status' => 404,
                    'head' => true
                ]);
        }
        else
            return $this->render([
                'status' => 404,
                'head' => true
            ]);
    }

    public function week(){
        //Get planning by type
        $object = null;
        $relatedObject = null;
        switch ($this->request->params['planningType']) {
            case 'eleve':
                $this->urlMaker->setSelector(new SelectorEleve($this->request->params['id']));
                $object = Eleves::getInstance()->getById($this->request->params['id']);
                if ($object)
                    $relatedObject = Promos::getInstance()->getById($object['P0']);
                break;
            case 'prof':
                $this->urlMaker->setSelector(new SelectorProf($this->request->params['id']));
                $object = Profs::getInstance()->getById($this->request->params['id']);
                break;
            case 'salle':
                $this->urlMaker->setSelector(new SelectorSalle($this->request->params['id']));
                $object = Salles::getInstance()->getById($this->request->params['id']);
                break;
            case 'promo':
                $this->urlMaker->setSelector(new SelectorPromo($this->request->params['id']));
                $object = Promos::getInstance()->getById($this->request->params['id']);
                break;
            default:
                return $this->render([
                    'status' => 404,
                    'head' => true
                ]);
                break;
        }

        //Date begin
        $daterBegin = (new Dater())->setTodayOrNextWorkingDay()->setFirstDayOfWeek(); //today
        if (isset($this->request->params['date'])) //date given
            $daterBegin->setStrDate($this->request->params['date'])->setFirstDayOfWeek();
        
        if ($daterBegin->checkValidTime()) {
            if ($object) { //Valid id
                //Date end
                $daterEnd = clone $daterBegin; //begin at dateBegin
                $daterEnd->setLastDayOfWeek();
    
                $this->urlMaker->setDateBegin($daterBegin->getDate());
                $this->urlMaker->setDateEnd($daterEnd->getDate());
    
                $dataParser = new DataParser();
                $dataParser->getRawFromUrl($this->urlMaker->getURL())->parse();
                $days = $dataParser->groupByDays($dataParser->getParsed());
                
                $title = (isset($object['NOM'])) ? $object['NOM'] : null;
    
                $this->render([
                    'data'  => [
                        'title' => $title,
                        'planningType' => $this->request->params['planningType'],
                        'itemId' => $this->request->params['id'],
                        'item' => $object,
                        'relatedItem' => $relatedObject,
                        'dateBegin' => $daterBegin->getDate(),
                        'todaysFirstDayOfWeek' => (new Dater())->setFirstDayOfWeek()->getDate(),
                        'dateEnd' => $daterEnd->getDate(),
                        'days' => $days,
                        'next' => $daterBegin->cloneDater()->setNextWeek()->getDate(),
                        'prev' => $daterBegin->cloneDater()->setPrevWeek()->getDate()
                    ]
                ]);
            }
            else
                return $this->render([
                    'status' => 404,
                    'head' => true
                ]);
        }
        else
            return $this->render([
                'status' => 404,
                'head' => true
            ]);
    }
}