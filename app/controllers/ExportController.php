<?php
namespace app\controllers;

use app\classes\Dater;
use app\classes\SelectorPromo;
use app\classes\SelectorSalle;
use app\classes\SelectorEleve;
use app\classes\SelectorProf;
use app\classes\URLMaker;
use app\classes\DataParser;
use app\classes\iCalPlanning;
use app\models\Promos;
use app\models\Eleves;
use app\models\Salles;
use app\models\Profs;
use lithium\action\Controller;
use lithium\storage\Cache;

class ExportController extends Controller{
    private $urlMaker;
    private $daterBegin;
    private $daterEnd;
    private $dataParser;
    private static $expiry = "+1 day";
    
    public function __construct(array $config){
        parent::__construct($config);
        
        if (isset($this->request->params['id'])) //id given in URL
            $this->urlMaker = new URLMaker();
        else
            die();
            
        $this->dataParser = new DataParser();
            
        $this->daterBegin = (new Dater())->setStrDate($this->request->params['dateBegin']);
        $this->daterEnd = (new Dater())->setStrDate($this->request->params['dateEnd']);
    }

    public function promo(){
        $promo = Promos::getInstance()->getById($this->request->params['id']);
        if ($promo && $this->daterBegin->checkValidTime() && $this->daterEnd->checkValidTime()) {
            $this->urlMaker->setSelector(new SelectorPromo($this->request->params['id']));
        
            $this->urlMaker->setDateBegin($this->daterBegin->getDate());
            $this->urlMaker->setDateEnd($this->daterEnd->getDate());
            
			$this->dataParser->getRawFromUrl($this->urlMaker->getURL())->parse();
			$data = $this->dataParser->getParsed();

            $ical = new iCalPlanning($data, 'promo-' . $this->request->params['id'] . '-' . $this->daterBegin->getDate() . '-' . $this->daterEnd->getDate() . '.ics', (isset($promo['NOM'])) ? $promo['NOM'] : null);
            
            // Empty response to avoid multiple header() sendings
            $this->_render['hasRendered'] = true;
            $this->response = null;
            
            $ical->returnCalendar();
        }
        
        else
            $this->render([
                'status' => 404,
                'head' => true
            ]);
    }
    
    public function eleve(){
        $eleve = Eleves::getInstance()->getById($this->request->params['id']);
        if ($eleve && $this->daterBegin->checkValidTime() && $this->daterEnd->checkValidTime()) {
            $this->urlMaker->setSelector(new SelectorEleve($this->request->params['id']));
        
            $this->urlMaker->setDateBegin($this->daterBegin->getDate());
            $this->urlMaker->setDateEnd($this->daterEnd->getDate());
            
            $this->dataParser->getRawFromUrl($this->urlMaker->getURL())->parse();
			$data = $this->dataParser->getParsed();
			
            $ical = new iCalPlanning($data, 'eleve-' . $this->request->params['id'] . '-' . $this->daterBegin->getDate() . '-' . $this->daterEnd->getDate() . '.ics', (isset($eleve['PRENOM']) && isset($eleve['NOM'])) ? $eleve['PRENOM'] . ' ' . $eleve['NOM'] : null);
            
            // Empty response to avoid multiple header() sendings
            $this->_render['hasRendered'] = true;
            $this->response = null;
            
            $ical->returnCalendar();
        }
        
        else
            $this->render([
                'status' => 404,
                'head' => true
            ]);
    }
    
    public function salle(){
        $salle = Salles::getInstance()->getById($this->request->params['id']);
        if ($salle && $this->daterBegin->checkValidTime() && $this->daterEnd->checkValidTime()) {
            $this->urlMaker->setSelector(new SelectorSalle($this->request->params['id']));
        
            $this->urlMaker->setDateBegin($this->daterBegin->getDate());
            $this->urlMaker->setDateEnd($this->daterEnd->getDate());
            
            $this->dataParser->getRawFromUrl($this->urlMaker->getURL())->parse();
			$data = $this->dataParser->getParsed();

            $ical = new iCalPlanning($data, 'salle-' . $this->request->params['id'] . '-' . $this->daterBegin->getDate() . '-' . $this->daterEnd->getDate() . '.ics', (isset($salle['NOM'])) ? 'Salle '. $salle['NOM'] : null);
            
            // Empty response to avoid multiple header() sendings
            $this->_render['hasRendered'] = true;
            $this->response = null;
            
            $ical->returnCalendar();
        }
        
        else
            $this->render([
                'status' => 404,
                'head' => true
            ]);
    }
    
    public function prof(){
        $prof = Profs::getInstance()->getById($this->request->params['id']);
        if ($prof && $this->daterBegin->checkValidTime() && $this->daterEnd->checkValidTime()) {
            $this->urlMaker->setSelector(new SelectorProf($this->request->params['id']));
        
            $this->urlMaker->setDateBegin($this->daterBegin->getDate());
            $this->urlMaker->setDateEnd($this->daterEnd->getDate());
            
            $this->dataParser->getRawFromUrl($this->urlMaker->getURL())->parse();
			$data = $this->dataParser->getParsed();

            $ical = new iCalPlanning($data, 'prof-' . $this->request->params['id'] . '-' . $this->daterBegin->getDate() . '-' . $this->daterEnd->getDate() . '.ics', (isset($prof['NOM'])) ? 'Prof '. $prof['NOM'] : null);
            
            // Empty response to avoid multiple header() sendings
            $this->_render['hasRendered'] = true;
            $this->response = null;
            
            $ical->returnCalendar();
        }
        
        else
            $this->render([
                'status' => 404,
                'head' => true
            ]);
    }
}