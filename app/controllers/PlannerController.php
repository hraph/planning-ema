<?php
namespace app\controllers;


use app\classes\DataParser;
use app\classes\Dater;
use app\classes\SelectorPromo;
use app\classes\SelectorSalle;
use app\classes\SelectorEleve;
use app\classes\SelectorProf;
use app\classes\URLMaker;
use app\models\Promos;
use app\models\Eleves;
use app\models\Salles;
use app\models\Profs;
use lithium\action\Controller;

class PlannerController extends Controller{
    public function __construct(array $config){
        parent::__construct($config);
        
        if (isset($this->request->params['id'])) //id given in URL
            $this->urlMaker = new URLMaker('planning_txt');
        else
            die();
    }
    
    public function index(){
        
        //Date begin
        $dater = (new Dater())->setTodayOrNextWorkingDay(); //today
        if (isset($this->request->params['date'])) //date given
            $dater->setStrDate($this->request->params['date']);

        if ($dater->checkValidTime()) {
            //Get planning by type
            $object = null;
            switch ($this->request->params['planningType']) {
                case 'eleve':
                    $this->urlMaker->setSelector(new SelectorEleve($this->request->params['id']));
                    $object = Eleves::getInstance()->getById($this->request->params['id']);
                    break;
                case 'prof':
                    $this->urlMaker->setSelector(new SelectorProf($this->request->params['id']));
                    $object = Profs::getInstance()->getById($this->request->params['id']);
                    break;
                case 'salle':
                    $this->urlMaker->setSelector(new SelectorSalle($this->request->params['id']));
                    $object = Salles::getInstance()->getById($this->request->params['id']);
                    break;
                case 'promo':
                    $this->urlMaker->setSelector(new SelectorPromo($this->request->params['id']));
                    $object = Promos::getInstance()->getById($this->request->params['id']);
                    break;
                default:
                    return $this->render([
                        'status' => 404,
                        'head' => true
                    ]);
                    break;
            }
            
            $this->render([
                'data'  => [
                    'itemId' => $this->request->params['id'],
                    'date' => $dater->getDate(),
                    'plannerType' => $this->request->params['planningType'],
                    'dateFormated' => date("Y-m-d", $dater->getTime()),
                    'planningType' => $this->request->params['planningType']
                ]
            ]);   
        } //validDate
        else
            return $this->render([
                'status' => 404,
                'head' => true
            ]);
    }
}
