<?php
namespace app\controllers;

use app\models\Promos;
use app\models\Salles;
use lithium\action\Controller;

class SelectController extends Controller{
    private $dataParser;

    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    public function index(){
        $this->_render['layout'] = "select"; //No layout

        $promos = Promos::getInstance()->getAll();
        $salles = Salles::getInstance()->getAll();

        return compact("promos", "salles");
    }
}