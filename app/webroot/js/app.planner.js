if (typeof app == "undefined")
    var app = {};

app.planner = (function(){
    var options = {
        timeslots: 2,
        visibleColumns: 5,
        columnLabels: [],
        plugins: [
          'collision',
          'mobile',
          'hourline'
        ]
      };
    var planner;
    var plannerItem = "#planner";
    
    function getUrl(planningType, id, date){
        return "/api/planner/get/" + planningType + "/" + id + "/" + date;
    }
    
    function getData(url, callback){
        if (typeof callback != "function")
            return;
            
        $.get(url,{}, callback, "json");
    }
    
    function setHistoryUrl(date){
        
    }
    
    function setLoading(state){
        if (state){
            $(".planner-container .loading").fadeIn(200);
        }
        else {
            $(".planner-container .loading").fadeOut(200);
        }
    }
    
    function initCalendar(date){
        //Initialize
        $(plannerItem).fullCalendar({
            editable: false, // Don't allow editing of events
            allDaySlot: false, //Hide allDay
            handleWindowResize: true,
            weekends: false, // Hide weekends
            defaultView: 'agendaWeek', // Only show week view
            defaultDate: date, //Begin date
            header: false, // Hide buttons/titles
            minTime: '07:00:00', // Start time for the calendar
            maxTime: '20:00:00', // End time for the calendar
            displayEventTime: true, // Display event time
            nowIndicator: true, //Red line
            
            //Style
            timeFormat: 'H:mm', //24h
            slotLabelFormat: 'H:mm',
            columnFormat: 'dddd D/M',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
            'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi',
            'Jeudi', 'Vendredi', 'Samedi'],
            eventBorderColor: "#000000",
            eventTextColor: "#000000",
            
            //Events
            windowResize: function(view) {
                if ($(window).width() < 514){
                    $(plannerItem).fullCalendar( 'changeView', 'basicDay' );
                } else {
                    $(plannerItem).fullCalendar( 'changeView', 'agendaWeek' );
                }
            }
            //eventRender: onEventRendering
        });
    }
    
    //Call when an event is being rendered
    function onEventRendering(event, element){
        if (typeof event.description != "undefined")
                element.find('.fc-title').append("<br/>" + event.description);    
    }
    
    function performDaysEvents(data){
        if (typeof data.content != "undefined") { 
            for (var dayKey in data.content) { //foreach days
                var day = data.content[dayKey];
                
                for (var eventKey in day) { //foreach events
                    var event = day[eventKey];
                    
                    var eventCard = {
                        id: event.id,
                        title: event.title,
                        description: event.content,
                        start: new Date(Date.parse(event.start)),
                        end: new Date(Date.parse(event.end)),
                        color: event.color
                    };
                    
                    //console.log(event);

                    $(plannerItem).fullCalendar('renderEvent', eventCard);
                }
            }
        }
        
        //Done
        setLoading(false);
    }
    
    function initHandlers(){
        
    }
    
    return {
        getDate: function(date){
            var planningType = $(plannerItem).data("planningtype");
            var id = $(plannerItem).data("id");
            
            if (typeof date != "undefined" && planningType != "undefined" && id != "undefined") {
                setLoading(true);
                getData(getUrl(planningType, id, date), performDaysEvents);
            }
        },
        init: function(){
            if ($(plannerItem).length > 0) {//If exists
                
                if (typeof $(plannerItem).data("date") != "undefined" && typeof $(plannerItem).data("dateformated") != "undefined") { //Get initial APIdata with the selected date
                    initCalendar($(plannerItem).data("dateformated"));
                    this.getDate($(plannerItem).data("date"));
                    
                }
                
                initHandlers();

            }
                
        }
    }
})()

$(document).ready(function(){
    app.planner.init();
});