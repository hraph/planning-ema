
var app = {};

app.ui = (function(){
    function search(){
        var container = ".search-container";
        var result = ".search-container .result";
        var input = ".searchByName";
        
            var debounce = function(callback, delay){
                var timer;
                return function(){
                    var args = arguments;
                    var context = this;
                    clearTimeout(timer);
                    timer = setTimeout(function(){
                        callback.apply(context, args);
                    }, delay)
                }
            }

            var checkEmptyInput = function(context) {
                if ($(context).val().length === 0) {
                    $(context).parent().next(result).fadeOut(); //Fadeout the result div next to the input
                }
            }
            
            var proceedInput = function(context){
                var c_input = $(context);
                var c_result = $(context).parent().next(result);
                var target = c_input.data('target');
                var key = c_input.data('key');

                if (typeof target != "undefined" && typeof key != "undefined" && (c_input.val()).length > 2) {
                    $.post("api/search/" + target + '/', {
                        "query": c_input.val()
                    }).done(function(data) {
                        try {
                            data = JSON.parse(data);
                        }
                        catch(e){
                            data = {}; //Invalid JSON input => manual set
                        }

                        c_result.empty().fadeIn();
                        if (typeof data.success != "undefined" && data.success && typeof data.content == "object") {
                            for(element in data.content) {
                                c_result.append(
                                    $('<a>').addClass('collection-item')
                                    .attr('href', target + '/' + data.content[element][key])
                                    .append(
                                        $("<li>").html(data.content[element].DET + " " + data.content[element].NOM + " " + data.content[element].PRENOM)
                                    )
                                );
                            }
                        }
                        else
                            c_result.append(
                                $('<li>').addClass('collection-item')
                                    .html("Aucun résultat")
                            );
                    })
                    .fail(function(e) {
                        c_result.empty().fadeIn();
                        c_result.append(
                                $('<li>').addClass('collection-item')
                                    .html("Erreur lors de la récupération")
                            );
                        console.log("Error", e); 
                    });
                };
            }

            $(input).keyup(function() {
                checkEmptyInput(this);
            });

            $(input).keydown(debounce(function() { //User input with throttle : launch search
                proceedInput(this);
            }, 200)); //Delay
            
            $(input).each(function(){//Onload check
                proceedInput(this); 
            });

            $(input).blur(function() {
                checkEmptyInput(this);
            });
    }
    
    function exportModal(){
        
        Date.prototype.getMonthFormatted  = function() {
            var month = this.getMonth() + 1;
            return month < 10 ? '0' + month : '' + month; // ('' + month) for string result
        }
        Date.prototype.getDateFormatted  = function() {
            var day = this.getDate();
            return day < 10 ? '0' + day : '' + day;
        }
        
        var dateBegin = new Date($("#export .options").data('begin'));
        var dateEnd = new Date($("#export .options").data('end'));

        $('#export .option').click(function(){
            $('#export .option').addClass('disabled');
            $(this).removeClass('disabled');
            
            proceedOptionButton(this);
        });
        
        var proceedCustom = function(){
            $("#export .custom .validate").click(function(){
                if (typeof $('#customFrom').val() != "undefined" && typeof $('#customTo').val() != "undefined") {
                    dateBegin = new Date($('#customFrom').val());
                    dateEnd = new Date($('#customTo').val());
                    proceedSummary();
                }
                else
                    alert("Veuillez entrer une date");
            });
        };
        
        var proceedOptionButton = function(elem){
            if (typeof $(elem).data('period') != "undefined") {
                switch($(elem).data('period')){
                    case 'custom':
                        $('#customFrom').val(dateBegin.getFullYear() + "-" + dateEnd.getMonthFormatted() + "-" + dateBegin.getDateFormatted());//Use ISO format y-m-d with 2 digits
                        $('#customTo').val(dateEnd.getFullYear() + "-" + dateEnd.getMonthFormatted() + "-" + dateEnd.getDateFormatted());
                        $('#export .custom').removeClass('hide').fadeIn();
                        $('#export .summary').fadeOut();
                        $('#export .to').fadeOut();
                        proceedCustom();
                        break;
                    case 'year':
                        dateEnd = new Date($("#export .options").data('endofyear'));
                        $('#export .custom').fadeOut(); //hide custom
                        proceedSummary();
                        break;
                    default:
                        dateEnd = new Date($("#export .options").data('end'));
                        $('#export .custom').fadeOut(); //hide custom
                        proceedSummary();
                        break;
                }
            }
        };
        
        var proceedSummary = function(){
            if (typeof dateBegin != "undefined" && typeof dateEnd != "undefined") {
                $('#export .summary .from').html(dateBegin.getDateFormatted() + "-" + dateBegin.getMonthFormatted() + "-" + dateBegin.getFullYear());
                $('#export .summary .to').html(dateEnd.getDateFormatted() + "-" + dateEnd.getMonthFormatted() + "-" + dateEnd.getFullYear());
                $('#export .summary').removeClass('hide').fadeIn();
                
                $('#export .to').removeClass('hide').fadeIn();
                proceedExport();
            }
        };
        
        var proceedExport = function(){
            type = $("#export .options").data('type');
            id = $("#export .options").data('id');
            begin = dateBegin.getFullYear() + dateBegin.getMonthFormatted() + dateBegin.getDateFormatted();
            end = dateEnd.getFullYear() + dateEnd.getMonthFormatted() + dateEnd.getDateFormatted();
            
            url = "/export/" + type + "/" + id + "/" + begin + "/" + end;
            full_url = window.location.origin + url;
            full_url_webcal = 'webcal://' + window.location.hostname + url;
            
            //QRCode
            $('#export .qrcode').html(
                $('<img>').attr('src', 'https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=' + full_url_webcal)
                .addClass("responsive-img")
            )
            
            //Buttons
            $('#export .export-button').click(function(){
                if (typeof $(this).data('format') != "undefined" && typeof $("#export .options").data('type') != "undefined" && typeof $("#export .options").data('id') != "undefined") {
                    format = $(this).data('format');
                
                    switch(format){
                        case 'ical':
                            window.open(full_url_webcal);
                            break;
                        case 'ics':
                            window.open(url);
                            break;
                        case 'google':
                            window.open('https://www.google.com/calendar/render?cid=' + full_url_webcal);
                            break;
                        default:
                            break;
                    }
                }
            });
        };
    }
    
    function init(){
        $(".modal-trigger").leanModal();
        if ($(".search-container").length > 0) //If exists
            search();
            
        if ($("#export").length > 0) //If exists
            $(".modal-trigger.exportModal").one("click", exportModal); //Execute only when modal's called
        
    }
    
    return {
        init: init
    }
})();

$(document).ready(function(){
    app.ui.init();
});