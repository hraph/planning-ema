<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2015, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

/**
 * The routes file is where you define your URL structure, which is an important part of the
 * [information architecture](http://en.wikipedia.org/wiki/Information_architecture) of your
 * application. Here, you can use _routes_ to match up URL pattern strings to a set of parameters,
 * usually including a controller and action to dispatch matching requests to. For more information,
 * see the `Router` and `Route` classes.
 *
 * @see lithium\net\http\Router
 * @see lithium\net\http\Route
 */
use lithium\net\http\Router;
use lithium\core\Environment;

Environment::set('production');


/**
 * ### Basic page routes
 *
 * Here, we are connecting `'/'` (the base path) to controller called `'Pages'`,
 * its action called `view()`, and we pass a param to select the view file
 * to use (in this case, `/views/pages/home.html.php`; see `app\controllers\PagesController`
 * for details).
 *
 * @see app\controllers\PagesController
 */
Router::connect('/', 'Select::index');

//Planner
//Router::connect('/{:planningType:prof|eleve|salle|promo}/{:id:\d+}/{:date:\d+}/planner', 'Planner::index');
//Router::connect('/{:planningType:prof|eleve|salle|promo}/{:id:\d+}/planner', 'Planner::index');

//Planning promo
Router::connect('/{:planningType:prof|eleve|salle|promo}/{:id:\d+}/{:date:\d+}/week', 'Planning::week');
Router::connect('/{:planningType:prof|eleve|salle|promo}/{:id:\d+}/{:date:\d+}', 'Planning::day');
Router::connect('/{:planningType:prof|eleve|salle|promo}/{:id:\d+}/week', 'Planning::week');
Router::connect('/{:planningType:prof|eleve|salle|promo}/{:id:\d+}', 'Planning::day');

//API
Router::connect('/api/search/eleve', 'API::getElevesByName');
Router::connect('/api/search/prof', 'API::getProfsByName');
Router::connect('/api/planning/get/{:id:\d+}/{:date:\d+}', 'API::getPlanningDate');
Router::connect('/api/planner/get/{:planningType:prof|eleve|salle|promo}/{:id:\d+}/{:date:\d+}', 'API::getPlannerDate');

//Export
Router::connect('/export/promo/{:id:\d+}/{:dateBegin:\d+}/{:dateEnd:\d+}', 'Export::promo');
Router::connect('/export/eleve/{:id:\d+}/{:dateBegin:\d+}/{:dateEnd:\d+}', 'Export::eleve');
Router::connect('/export/salle/{:id:\d+}/{:dateBegin:\d+}/{:dateEnd:\d+}', 'Export::salle');
Router::connect('/export/prof/{:id:\d+}/{:dateBegin:\d+}/{:dateEnd:\d+}', 'Export::prof');

/**
 * ### Database object routes
 *
 * The routes below are used primarily for accessing database objects, where `{:id}` corresponds to
 * the primary key of the database object, and can be accessed in the controller as
 * `$this->request->id`.
 *
 * If you're using a relational database, such as MySQL, SQLite or Postgres, where the primary key
 * is an integer, uncomment the routes below to enable URLs like `/posts/edit/1138`,
 * `/posts/view/1138.json`, etc.
 */
//Router::connect('/{:controller}/{:action}/{:id:\d+}.{:type}', array('id' => null));
//Router::connect('/{:controller}/{:action}/{:id}');

/**
 * If you're using a document-oriented database, such as CouchDB or MongoDB, or another type of
 * database which uses 24-character hexidecimal values as primary keys, uncomment the routes below.
 */
// Router::connect('/{:controller}/{:action}/{:id:[0-9a-f]{24}}.{:type}', array('id' => null));
// Router::connect('/{:controller}/{:action}/{:id:[0-9a-f]{24}}');

/**
 * ### Default controller/action routes
 *
 * Finally, connect the default route. This route acts as a catch-all, intercepting requests in the
 * following forms:
 *
 * - `/foo/bar`: Routes to `FooController::bar()` with no parameters passed.
 * - `/foo/bar/param1/param2`: Routes to `FooController::bar('param1, 'param2')`.
 * - `/foo`: Routes to `FooController::index()`, since `'index'` is assumed to be the action if none
 *   is otherwise specified.
 *
 * In almost all cases, custom routes should be added above this one, since route-matching works in
 * a top-down fashion.
 */
//Router::connect('/{:controller}/{:action}/{:args}');

?>