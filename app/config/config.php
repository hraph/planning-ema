<?php

return [
    "app_name"              => "Planning EMA",
    "view_date_format"      => "l j/m/y",
    "view_date_format_week" => "j/m/y",
    "cgidb"                 => "http://webdfd.mines-ales.fr/cybema/cgi-bin/cgiempt.exe",
    "cgihtml"               => "http://webdfd.mines-ales.fr/cybema/cgi-bin/cgihtml.exe",
    "cgiprof"               => "http://webdfd.mines-ales.fr/cybema/cgi-bin/cgiprof.exe",
    "cgieleve"              => "http://webdfd.mines-ales.fr/cybema/cgi-bin/cgieleve.exe"
];