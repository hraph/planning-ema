<div class="container">
    <div class="planning">
        <div class="planning-elements">
            <div class="controls row">
                <div class="col s12 l9 row">
                    <div class="control-padding col s12 l4">
                        <a href="<?= $this->url(['Planning::day', 'planningType' => $planningType, 'id' => $itemId, 'date' => date(app\classes\Dater::$dateFormat, time())])?>" class="col s12 waves-effect waves-light btn light-blue lighten-2 <?=(date(app\classes\Dater::$dateFormat, time()) == $date) ? "disabled" : "";?>">Aujourd'hui</a>
                    </div> 
                    <div class="control-padding col s12 l4">
                        <a href="<?= $this->url(['Planning::week', 'planningType' => $planningType, 'id' => $itemId, 'date' => $date])?>" class="col s12 waves-effect waves-light btn green lighten-3">Vue hebdomadaire</a>
                    </div>
                    <div class="control-padding col s12 l1"> 
                        <a href="#exportModal" data-target="exportModal" class="col s12 modal-trigger exportModal waves-effect waves-light btn grey darken-3"><i class="material-icons">share</i><span class="hide-on-large-only"> Export</span></a>
                    </div>
                </div>
                <div class="col s12 l3 row">
                    <div class="control-padding col s6">
                        <a href="<?= $this->url(['Planning::day', 'planningType' => $planningType, 'id' => $itemId, 'date' => $prev])?>" class="col s12 waves-effect waves-light btn grey"><i class="material-icons">keyboard_arrow_left</i></a>
                    </div>
                    <div class="control-padding col s6">
                        <a href="<?= $this->url(['Planning::day', 'planningType' => $planningType, 'id' => $itemId, 'date' => $next])?>" class="col s12 waves-effect waves-light btn grey"><i class="material-icons">keyboard_arrow_right</i></a>
                    </div>
                </div>
            </div>
            <?php if (isset($date)): ?>
                <ul class="collection with-header">
                    <li class="collection-header">
                        <h4>
                            <?php 
                                if (isset($item) && isset($relatedItem)): //Case eleve and promo related
                                    if (isset($item['PRENOM']) && isset($item['NOM'])):
                                        echo '(' . $item['PRENOM'] . ' '. $item['NOM'];
                                        if (isset($relatedItem['NOM'])) echo ' - ' . $relatedItem['NOM'];
                                        echo ') ';
                                    endif;
                                elseif (isset($item) && isset($item['NOM'])):
                                    echo '(' . $item['NOM'] . ') ';
                                endif;?>
                            <?=date(app\classes\Config::getInstance()->get('view_date_format'), strtotime($date))?>
                        </h4>
                    </li>
                    <?php if(@count($day['content']) > 0):
                        $colorReader = new app\classes\ColorReader();
                        foreach ($day['content'] as $element){
                            echo $this->_view->render(
                        	   ['element' => ($planningType == "salle" || $planningType == "prof") ? 'planning/dayElementByPromo' : 'planning/dayElement'], compact('element', 'colorReader') //Custom view for prof and salle
                            );
                        } //endforeach
                    else: ?>
                        <li class="collection-item element">
                            Aucun élément à afficher
                        </li>
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
        </div>
        
        <?php echo $this->_view->render(
    	   ['element' => 'planning/exportModal'], [
    	        "type" => $planningType,
    	        "date" => $date,
    	        "id"   => $itemId
    	   ]
        ); ?>
        
    </div>
</div>