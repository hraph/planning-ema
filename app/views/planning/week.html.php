<div class="row">
    <div class="planning">
        <div class="planning-elements col s12 m10 offset-m1">
            <div class="controls row">
                <div class="col s12 l9 row no-padding">
                    <div class="control-padding col s12 l4">
                        <a href="<?= $this->url(['Planning::week', 'planningType' => $planningType, 'id' => $itemId, 'date' => date(app\classes\Dater::$dateFormat, time())])?>" class="col s12 waves-effect waves-light btn light-blue lighten-2 <?=($todaysFirstDayOfWeek === $dateBegin) ? "disabled" : "";?>">Semaine courante</a>
                    </div> 
                    <div class="control-padding col s12 l4">
                        <a href="<?= $this->url(['Planning::day', 'planningType' => $planningType, 'id' => $itemId, 'date' => $dateBegin])?>" class="col s12 waves-effect waves-light btn green lighten-3">Vue quotidienne</a>
                    </div>
                    <div class="control-padding col s12 l1"> 
                        <a href="#exportModal" data-target="exportModal" class="col s12 modal-trigger exportModal waves-effect waves-light btn grey darken-3"><i class="material-icons">share</i><span class="hide-on-large-only"> Export</span></a>
                    </div>
                </div>
                <div class="col s12 l3 row no-padding">
                    <div class="control-padding col s6">
                        <a href="<?= $this->url(['Planning::week', 'planningType' => $planningType, 'id' => $itemId, 'date' => $prev])?>" class="col s12 waves-effect waves-light btn grey"><i class="material-icons">keyboard_arrow_left</i></a>
                    </div>
                    <div class="control-padding col s6">
                        <a href="<?= $this->url(['Planning::week', 'planningType' => $planningType, 'id' => $itemId, 'date' => $next])?>" class="col s12 waves-effect waves-light btn grey"><i class="material-icons">keyboard_arrow_right</i></a>
                    </div>
                </div>
            </div>
            <?php if (isset($dateBegin) && isset($dateEnd)): ?>
                <div class="week-dates">
                        <h4>
                            <?php 
                                if (isset($item) && isset($relatedItem)): //Case eleve/prof and promo related
                                    if (isset($item['PRENOM']) && isset($item['NOM'])):
                                        echo '(' . $item['PRENOM'] . ' '. $item['NOM'];
                                        if (isset($relatedItem['NOM'])) echo ' - ' . $relatedItem['NOM'];
                                        echo ') ';
                                    endif;
                                elseif (isset($item) && isset($item['NOM'])):
                                    echo '(' . $item['NOM'] . ') ';
                                endif;?>
                            <?=date(app\classes\Config::getInstance()->get('view_date_format_week'), strtotime($dateBegin))?> - <?=date(app\classes\Config::getInstance()->get('view_date_format_week'), strtotime($dateEnd))?>
                        </h4>
                </div>
                <table class="week-container highlight bordered">
                    
                    <?php if(count($days) > 0):
                        foreach ($days as $day){ ?>
                        <tr class="week-day">
                            <td class="week-day-date <?=(date(app\classes\Dater::$dateFormat, time()) == $day['date']) ? "today" : "";?> col s1">
                                <?=date(app\classes\Config::getInstance()->get('view_date_format'), strtotime($day['date']))?>
                            </td>
                            <td class="week-day-content col s11">
                                <?php if(count($day['content']) > 0):
                                    $colorReader = new app\classes\ColorReader();
                                    foreach ($day['content'] as $element){
                                        echo $this->_view->render(
                                    	   ['element' => ($planningType == "salle" || $planningType == "prof") ? 'planning/weekElementByPromo' : 'planning/weekElement'], compact('element', 'colorReader') //Custom view for prof and salle
                                        );
                                    } //endforeach
                                else: ?>
                                    <div class="week-day-element">
                                        Aucun élément à afficher
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php } //endforeah
                    else: ?>
                        <div>
                            Aucun élément à afficher
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        
        <?php echo $this->_view->render(
    	   ['element' => 'planning/exportModal'], [
    	        "type" => $planningType,
    	        "date" => $dateBegin,
    	        "id"   => $itemId
    	   ]
        ); ?>
        
    </div>
</div>