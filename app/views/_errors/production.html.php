<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2015, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */
use lithium\analysis\Debugger;
use lithium\analysis\Inspector;
$exception = $info['exception'];
?>

<?php if ($code = $exception->getCode()): ?>
	<h4 >Error <?=$code;?></h4>
<?php endif ?>
<?php if (preg_match("/AppException/", get_class($exception))): //Not a standard exception ?>
    <h5><?=$exception->getMessage();?></h5>
<?php endif; ?>