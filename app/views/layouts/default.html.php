<?php echo $this->_render("element", "head"); ?>

<body>
    <?php echo $this->_render("element", "header"); ?>
    
    <div class="main">
    	<?php echo $this->content(); ?>
    </div>
    
    <?php echo $this->_render("element", "footer"); ?>
</body>
</html>