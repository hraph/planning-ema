<?php
use lithium\core\Libraries;
$path = Libraries::get(true, 'path');
?>
<?= $this->_render('element', 'head'); ?>
<?= $this->_render('element', 'header'); ?>
    <div class="content">
        <h3>Something went wrong!</h3>
        <?php echo $this->content(); ?>
    </div>
