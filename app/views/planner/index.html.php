<?php echo $this->html->style([
    "fullcalendar.min",
    "fullcalendar.theme"
]); ?>

<?php echo $this->html->script([
    "moment",
    "fullcalendar.min",
    "app.planner"
]);
?>
<div class="row">
    <div class="planning">
        <div class="planning-elements col s12 m10 offset-m1">
            <div class="controls row">
                <div class="col s12 l9 row no-padding">
                    <div class="control-padding col s12 l4">
                        <a href="<?= $this->url(['Planning::week', 'planningType' => $planningType, 'id' => $itemId, 'date' => date(app\classes\Dater::$dateFormat, time())])?>" class="col s12 waves-effect waves-light btn light-blue lighten-2 <?=($todaysFirstDayOfWeek === $dateBegin) ? "disabled" : "";?>">Semaine courante</a>
                    </div> 
                    <div class="control-padding col s12 l4">
                        <a href="<?= $this->url(['Planning::day', 'planningType' => $planningType, 'id' => $itemId, 'date' => $dateBegin])?>" class="col s12 waves-effect waves-light btn green lighten-3">Vue quotidienne</a>
                    </div>
                    <div class="control-padding col s12 l1"> 
                        <a href="#exportModal" data-target="exportModal" class="col s12 modal-trigger exportModal waves-effect waves-light btn grey darken-3"><i class="material-icons">share</i><span class="hide-on-large-only"> Export</span></a>
                    </div>
                </div>
                <div class="col s12 l3 row no-padding">
                    <div class="control-padding col s6">
                        <a href="#" class="col s12 waves-effect waves-light btn grey"><i class="material-icons">keyboard_arrow_left</i></a>
                    </div>
                    <div class="control-padding col s6">
                        <a href="#" class="col s12 waves-effect waves-light btn grey"><i class="material-icons">keyboard_arrow_right</i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="planner-container">
            <div class="loading valign-wrapper hidden">
                <div class="preloader-wrapper big active">
                    <div class="spinner-layer spinner-blue-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="planner" data-date="<?= $date ?>" data-dateformated="<?= $dateFormated ?>" data-planningtype="<?= $planningType ?>" data-id="<?= $itemId ?>"></div>
        </div>
        
        <?php echo $this->_view->render(
    	   ['element' => 'planning/exportModal'], [
    	        "type" => $planningType,
    	        "date" => $dateBegin,
    	        "id"   => $itemId
    	   ]
        ); ?>
        
    </div>
</div>