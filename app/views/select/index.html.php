<div class="container">
    <div class="card-panel grey lighten-3">
        Recherchez un planning ou sélectionnez un élément
    </div>
    <div class="select-container row">
        <div class="select col s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header z-depth-1 active"><i class="material-icons">account_circle</i><b>Planning personnalisé étudiant</b></div>
                    <div class="collapsible-body">
                        <div class="search-container collapsible-content col s12 grey lighten-4">
                            <div class="form  col s12 m10 offset-m1">
                                <input type="text" class="searchByName" id="searchByNameEleve" data-target="eleve" data-key="EV" placeholder="Rechercher par nom OU prénom d'étudiant"/>
                            </div>
                            <ul class="result collection col s12 m10 offset-m1" id="searchResultEleve">
                                
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header z-depth-1"><i class="material-icons">group</i><b>Promos</b></div>
                    <div class="collapsible-body">
                        <div class="promos-container collapsible-content col s12 grey lighten-4">
                            <ul class="collection col s12 m10 offset-m1">
                                <?php if (count($promos) > 0)
                                    foreach($promos as $promo): ?>
                                    <?php if (isset($promo['NOM']) && !empty($promo['NOM'])): ?>
                                        <a href="<?= $this->url(['Planning::day', 'planningType' => 'promo', 'id' => $promo['P0']])?>" class="collection-item">
                                            <li><?php echo $promo['NOM'] ?></li>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header z-depth-1"><i class="material-icons">store</i><b>Salles</b></div>
                    <div class="collapsible-body">
                        <div class="promos-container collapsible-content col s12 grey lighten-4">
                            <ul class="collection col s12 m10 offset-m1">
                                <?php if (count($salles) > 0)
                                    foreach($salles as $salle): ?>
                                    <?php if (isset($salle['NOM']) && !empty($salle['NOM'])): ?>
                                        <a href="<?= $this->url(['Planning::day', 'planningType' => 'salle', 'id' => $salle['SA']])?>" class="collection-item">
                                            <li><?php echo $salle['NOM'] ?></li>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header z-depth-1"><i class="material-icons">accessibility</i><b>Profs</b></div>
                    <div class="collapsible-body">
                        <div class="search-container collapsible-content col s12 grey lighten-4">
                            <div class="form  col s12 m10 offset-m1">
                                <input type="text" class="searchByName" id="searchByNameProf" data-target="prof" data-key="PR" placeholder="Rechercher par nom OU prénom de professeur"/>
                            </div>
                            <ul class="result collection col s12 m10 offset-m1" id="searchResultProf">
                                
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        
        <div class="col s12">
            <div class="card-panel cyan accent-4 lighten-1 white-text" style="overflow:hidden;">
                <div class="col s1 hide-on-med-and-down"><i class="material-icons" style="font-size: 40px;">info</i></div>
                <div class="col s11">
                    Exportez votre planning personnalisé vers votre smartphone sur chaque page de planning !<br/>
                    Celui-ci se synchronisera automatiquement.
                </div>
            </div>
        </div>
    </div>
</div>