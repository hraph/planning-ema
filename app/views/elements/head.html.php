<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="description" content="Retrouvez le planning officiel de l'école des Mines d'Alès">
<meta name="keywords" content="planning, calendrier, école, mines, alès, mines alès, promo, EMA">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no, user-scalable=no">

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#00b8de">
<meta name="msapplication-TileColor" content="#00b8de">
<meta name="theme-color" content="#ffffff">

<?php echo $this->html->style([
    "style",
    "materialize.min",
    "https://fonts.googleapis.com/icon?family=Material+Icons"
]); ?>

<?php echo $this->html->script([
    "jquery",
    "materialize.min",
    "app"
]);
?>
<title>
    <?php echo app\classes\Config::getInstance()->get('app_name');
    echo (isset($title) && !empty($title)) ? " - " . ucfirst($title) : "" ; ?>
</title>
</html>