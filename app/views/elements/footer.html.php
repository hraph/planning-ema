<!-- Modal Structure -->
<div id="infoModal" class="modal">
    <div class="modal-content row">
        <h4>A Propos</h4>
        <div class="col s6 logo">
            <?= $this->html->image('logo_imt.jpg', ["class" => "responsive-img"]);?>
        </div>
        <div class="col s6">
            <h5>Planning des enseignements d'IMT Mines Alès</h5>
            <p>Visionnez votre emploi du temps sur ordinateur ou mobile.</p>
            <p>Pour toute question, remarque ou dysfonctionnement : contactez <a href="mailto:jean-paul.veuillez@mines-ales.fr">jean-paul.veuillez@mines-ales.fr</a></p>
            <p><a href="http://www.mines-ales.fr/" target="_blank">IMT Mines Alès</a> est une école du groupe <a href="https://www.imt.fr/" target="_blank">Institut Mines Télécom</a>.</p>
            <hr/>
            <p>© 2016 - <?=date("Y")?> | Réalisation : <a href="https://github.com/hraph/" target="_blank">Raphaël Haltz</a></p>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
    </div>
</div>