<?php $dater = (new app\classes\Dater())->setStrDate($date); ?>
<!-- Modal Structure -->
<div id="exportModal" class="modal modal-fixed-footer">
    <div class="modal-content row">
        <h4>Exporter le calendrier</h4>
        <div class="col s12 no-padding" id="export">
            <div class="options" data-begin="<?=$dater->setFirstDayOfWeek()->getTime()?>000" data-end="<?=$dater->setLastDayOfWeek()->getTime()?>000" data-endofyear="<?=$dater->setNext31August()->getTime()?>000" data-type="<?=$type?>" data-id=<?=$id?>>
                <h5>Options</h5>
                <p>Que souhaitez vous exporter ?</p>
                <div class="col s12 no-padding">
                    <div class="col s12 m6 row">
                        <a class="option disabled col s12 waves-effect waves-light btn red" data-period="week">Semaine courante</a>
                    </div>
                    <div class="col s12 m6 row">
                        <a class="option disabled col s12 waves-effect waves-light btn blue lighten-1" data-period="year">Année entière</a>
                    </div>
                </div>
                <div class="col s12 no-padding row">
                    <div class="col s12">
                        <a class="option disabled col s12 waves-effect waves-light btn amber lighten-2" data-period="custom" style="word-break: break-all;">Date personnalisée</a>
                    </div>
                </div>
                <div class="custom hide row col s12">
                    <div class="col s12 m5">
                        Du <input type="date" id="customFrom"/>
                    </div>
                    <div class="col s12 m5">
                        Au <input type="date" id="customTo"/>
                    </div>
                    <div class="col s12 m2">
                         <a class="validate col s12 waves-effect waves-light btn">Ok</a>
                    </div>
                </div>
            </div>
            <div class="summary hide col s12">
                <div class="date">
                    Export du <span class="from" style="font-weight: bold;"></span> au <span class="to" style="font-weight: bold;"></span>
                </div>
                <div class="count hide">
                    Nombre d'évènements : <span class="number" style="font-weight: bold;"></span>
                </div>
            </div>
            <div class="to hide col s12">
                <h5>Exporter vers</h5>
                <div class="row">
                    <div class="col s12 m4 row">
                        <a class="export-button col s12 waves-effect waves-light btn blue lighten-2" data-format="ical">Format iCal</a>
                    </div>
                    <div class="col s12 m4 row">
                        <a class="export-button col s12 waves-effect waves-light btn light-green" data-format="google">Google Calendar</a> 
                    </div>
                    <div class="col s12 m4 row">
                        <a class="export-button col s12 waves-effect waves-light btn brown lighten-2" data-format="ics">Fichier .ics</a>
                    </div>
                </div>
                
                <div class="qrcode col s12 center"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
</div>