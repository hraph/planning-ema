<li class="collection-item element"
    <?php if(isset($element['CFOND']) && $element['CFOND'] != 0): ?>
        style="background: <?=$colorReader->setInt($element['CFOND'])->getHex()?>"
    <?php endif; ?>
>
    <b>
        <?= $element['COURS'] ?>
    </b>
    <i>
        <span class="grey-text text-darken-1"><?= date("H:i", strtotime($element['HD'])) ?> - <?= date("H:i", strtotime($element['HF'])) ?></span>
    </i>
    <br>
    <?php if (isset($element['P0CLE']) && isset($element['PROMO'])): ?>
        <a href="<?= $this->url(['Planning::day', 'planningType' => 'promo', 'id' => $element['P0CLE'], 'date' => $element['DATE']])?>">Promo <?= $element['PROMO'] ?></a><br/>
    <?php endif; ?>
    
    <?php if ($element['PROF'] != "-" && isset($element['PRCLE'])): ?>
        <a href="<?= $this->url(['Planning::day', 'planningType' => 'prof', 'id' => $element['PRCLE'], 'date' => $element['DATE']])?>" class="teal-text text-darken-1"><?= $element['PROF'] ?></a>
    <?php endif; ?> 
    <?php if ($element['SALLE'] != "-" && isset($element['SACLE'])): ?>
        <a href="<?= $this->url(['Planning::day', 'planningType' => 'salle', 'id' => $element['SACLE'], 'date' => $element['DATE']])?>" class="red-text text-darken-2">Salle <?= $element['SALLE'] ?></a>
    <?php endif; ?>
    <br>
    <?php if ($element['GROUPE'] != "-"): ?>
        <span class="blue-grey-text text-darken-2">Groupe <?= $element['GROUPE'] ?></span> 
    <?php endif; ?>
    <?php if (!empty($element['LANOTE'])): ?>
        <span class="grey-text text-darken-1">(<?= $element['LANOTE'] ?>)</span>
    <?php endif; ?>
</li>