<div class="header-bar">
    <nav id="nav-actions">
        <div class="nav-wrapper pink darken-3">
            <a href="<?= $this->url(['Select::index']) ?>" class="waves-effect waves-light brand-logo">
                <i class="material-icons hide-on-small-only">event</i><?php echo app\classes\Config::getInstance()->get('app_name') ?>
            </a>
            <a href="#infoModal" class="waves-effect waves-light modal-trigger button-collapse mobile-nav-button" data-target="infoModal">
                <i class="material-icons">info</i>
            </a>

            <ul class="right hide-on-med-and-down" id="desktop-nav">
                <li><a href="#infoModal" class="waves-effect waves-light modal-trigger" data-target="infoModal"><i class="material-icons">info</i></a></li>
            </ul>
        </div>
    </nav>
</div>