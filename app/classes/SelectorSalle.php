<?php

namespace app\classes;


class SelectorSalle implements iSelector
{
    protected static $key = "sacleunik";
    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getKey()
    {
        return self::$key;
    }
}