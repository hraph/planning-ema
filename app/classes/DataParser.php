<?php
namespace app\classes;
use lithium\storage\Cache;

class DataParser {
	protected $rawContent;
	protected $parsedContent;
	protected static $timeout = 2;

	public function getUrl($url){
		$context = stream_context_create(array(
		    'http' => array(
		        'timeout' => self::$timeout
		        )
		    )
		);
		$content = @file_get_contents($url, false, $context);
		if ($content === FALSE)
			throw new AppException("Impossible de se connecter au serveur de l'EMA");
		else return $content;
	}
	
	public function getRawFromUrl($url, $cache = false, $expiry = '+1 day'){
		if (!$cache)
			$this->rawContent = self::getUrl($url);
		else {
			$fileMd5 = md5($url);
			$read = Cache::read('datadb', $fileMd5);
			if ($read == null)
				Cache::write('datadb', $fileMd5, $this->getRawFromUrl($url, false)->rawContent, $expiry); //Save to cache raw (got from same method without cache: IMPORTANT)
			else
				$this->rawContent = $read;
		}
		return $this;
	}

	public function setRaw($raw){
		$this->rawContent = $raw;
		return $this;
	}

	public function getParsed(){
		return $this->parsedContent;
	}

	public function getParsedJson(){
		return json_encode($this->parsedContent);
	}

	public function parse() {
		if (is_null($this->rawContent)) return false; //Unset raw

		$returnData = [];
		$lines = explode(PHP_EOL, $this->rawContent); //Split into lines

		foreach ($lines as $line) {
			if (empty($line) || trim($line) == 'EOT') break; //Ignore last line

			$lineData = [];
			$lineElements = explode(';', $line); //Split elements of each lines
			//Elements are alternatively key;value;key;value...

			foreach ($lineElements as $keyElement => $element) {
				//Each odd (impair) keyElement is the key
				//Each even (pair) keyElement is the value

				if (($keyElement % 2) == 0) //Odd => Key
					$key = trim($element); //Add key
				else //Event => Value
					$value = utf8_encode(trim($element));

				//Proceed data
				if (isset($key) && isset($value)){ //Got a complete key => value
					$lineData[$key] = $value; //Add to lineData
					unset($key, $value);
				}
				elseif (isset($key)) //Only got a key
					$lineData[$key] = NULL; //Add to lineData
			}

			$returnData[] = $lineData; //Add into returnData 	
		}

		$this->parsedContent = $returnData;
		return true;
	}
	
	private function sortDayEvents($eventA, $eventB){
        if (!isset($eventA["HD"]) || !isset($eventB["HD"]))
            return 0;
        return ($eventA["HD"] > $eventB["HD"]);
    }

    public function groupByDays($data){
        $return = [];
        foreach ($data as $dat){
            if (isset($dat['DATE'])) { //Has date
                $return[$dat['DATE']]['date'] = $dat['DATE']; //Save date
                $return[$dat['DATE']]['content'][] = $dat; //Save data
            }
        }
    
        foreach ($return as $key => $value) { //Sort each days
            usort($return[$key]["content"], array($this, 'sortDayEvents')); //Sort events
        }
        return $return;
    }
}