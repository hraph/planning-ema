<?php
namespace app\classes;

class Dater {
	protected $time;
	public static $dateFormat = 'Ymd';
	protected static $nonWorkingDays = [7];
	protected static $firstDayOfWeek = "monday";
	protected static $firstDayOfWeekIndex = 1;
	protected static $lastDayOfWeek = "saturday";
	protected static $lastDayOfWeekIndex = 6;

	public function __construct(){
		$this->time = time();
	}

	public function setTodayOrNextWorkingDay(){
		$this->time = time();
		if (in_array(date('N', $this->time), self::$nonWorkingDays)) //non working day
			$this->setNextDay();
		return $this;
	}

	public function setTime($time){
		$this->time = $time;
		return $this;
	}

	public function setStrDate($date){
		$this->time = strtotime($date);
		return $this;
	}

	public function setFirstDayOfWeek(){
		if (date('N', $this->time) != self::$firstDayOfWeekIndex) //time is already on firstDayOfWeek
			$this->time = strtotime('last '. self::$firstDayOfWeek, $this->time);
		return $this;
	}

	public function setLastDayOfWeek(){
		if (date('N', $this->time) != self::$lastDayOfWeekIndex)//time is already on lastDayOfWeek
			$this->time = strtotime('next '. self::$lastDayOfWeek, $this->time);
		return $this;
	}

	public function getDate(){
		return date(self::$dateFormat, $this->time);
	}

	public function setPrevDay(){
		$this->time = strtotime('-1 day', $this->time);
		if (in_array(date('N', $this->time), self::$nonWorkingDays)) //non working day
			$this->setPrevDay();
		return $this;
	}

	public function setNextDay(){
		$this->time = strtotime('+1 day', $this->time);
		if (in_array(date('N', $this->time), self::$nonWorkingDays)) //non working day
			$this->setNextDay();
		return $this;
	}

	public function setPrevWeek(){
		$this->time = strtotime('-1 week', $this->time);
		return $this;
	}

	public function setNextWeek(){
		$this->time = strtotime('+1 week', $this->time);
		return $this;
	}
	
	public function setNext31August(){
		$this->time = (date("m", time()) >= 9) ? strtotime(date("Y", strtotime("+1 years")) . "0831") : strtotime(date("Y", time()) . "0831");
		return $this;
	}

	public function getTime(){
		return $this->time;
	}

	public function checkValidTime(){
		return (is_null($this->time) || !$this->time) ? false : true;
	}

	public function cloneDater(){
		return clone $this;
	}
}