<?php

namespace app\classes;


class SelectorProf implements iSelector
{
    protected static $key = "prcleunik";
    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getKey()
    {
        return self::$key;
    }
}