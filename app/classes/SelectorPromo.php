<?php

namespace app\classes;


class SelectorPromo implements iSelector
{
    protected static $key = "p0cleunik";
    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getKey()
    {
        return self::$key;
    }
}