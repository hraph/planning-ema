<?php

namespace app\classes;


class Config {
    private $config = array();
    private $appDir;
    private static $instance;

    public static function getInstance(){
        if(is_null(self::$instance))
            self::$instance = new Config();
        return self::$instance;
    }

    private function __construct(){
        //AppDir
        if(!defined('__DIR__')) {
            define('__DIR__', dirname(__FILE__));
        }
        $this->appDir = dirname(__DIR__); //Parent of controllers folder

        //Config
        if (file_exists($this->appDir . '/config/config.php'))
            $this->config = require($this->appDir . '/config/config.php');
        else
            throw new Exception("ERROR: File config not found");
    }

    public function getAppDir(){
        return dirname(__DIR__);
    }

    public function get($key){
        return (isset($this->config[$key]) ? $this->config[$key] : false);
    }

    public function set($key, $value){
        $this->config[$key] = $value;
    }
}