<?php

namespace app\classes;


class URLMaker
{
    const DEFAULT_DB = "cgidb";
    const DEFAULT_TYPE = "planning_txt";

    private $db;
    private $type;
    private $dateBegin;
    private $dateEnd;
    private $selector;

    public function __construct($type = self::DEFAULT_TYPE, $db = self::DEFAULT_DB){
        $this->db = $db;
        $this->type = $type;
    }

    public function setType($type){
        $this->type = $type;
        return $this;
    }

    public function setDB($DB){
        $this->db = $DB;
        return $this;
    }

    public function setDate($begin, $end = null){
        $this->dateBegin = $begin;
        $this->dateEnd = (!is_null($end) ? $end : $begin); //If has no end => end = begin
    }

    public function setDateBegin($date){
        $this->dateBegin = $date;
    }

    public function setDateEnd($date){
        $this->dateEnd = $date;
    }

    public function setSelector($selector){
        $this->selector = $selector;
    }

    public function getURL(){
        $dbUrl = Config::getInstance()->get($this->db);

        $commonUrl = $dbUrl . "?TYPE=" . $this->type;

        if (isset($this->dateBegin)) $commonUrl .= "&DATEDEBUT=" . $this->dateBegin;
        if (isset($this->dateEnd)) $commonUrl .= "&DATEFIN=" . $this->dateEnd;

        if (isset($this->selector) && $this->selector instanceof iSelector) $commonUrl .= "&TYPECLE=" . $this->selector->getKey() . "&VALCLE=" . $this->selector->getValue();

        return $commonUrl;
    }
}