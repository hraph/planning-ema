<?php

namespace app\classes;


interface iSelector
{
    public function __construct($value);
    public function getValue();
    public function getKey();
}