<?php

namespace app\classes;


class iCalPlanning
{
    private static $tz = "Europe/Paris";
    private $ical;
    private $data;
    private $calName;
    
    public function __construct($data, $filename, $calName = ""){
        require_once('../../vendor/kigkonsult/icalcreator/autoload.php'); //as Li3 dont use the composer autoload : use directly icalcreator autoload
        
        $this->data = $data;
        $this->calName = $calName;
        
        $this->ical = new \kigkonsult\iCalcreator\vcalendar([
            "unique_id" => "planning-ema@gmx.com",
            "TZID"      => self::$tz,
            "filename"  => $filename
        ]);
        
        //Header
        $this->setHeader();
        
        //Body
        $this->setDataEvents();
    }
    
    public function returnCalendar(){
        if (!is_null($this->ical))
            return $this->ical->returnCalendar();
        return false;
    }
    
    private function setHeader(){
        if (is_null($this->ical)) return false;
        
        $this->ical->setProperty("METHOD", "PUBLISH");
        
        $this->ical->setProperty("X-WR-TIMEZONE", self::$tz);
        $this->ical->setProperty("X-XR-CALNAME", "Planning EMA " . $this->calName);
        $this->ical->setProperty("X-WR-CALDESC", "Planning EMA " . $this->calName);
        
        $vtimezone = $this->ical->newComponent("vtimezone");
        $vtimezone->setProperty("TZID", self::$tz);
    }
    
    private function setDataEvents(){
        if (is_null($this->ical) || !is_array($this->data)) return false;
        
        foreach($this->data as $data){
            $this->setEvent($data);
        }
        return true;
    }
    
    private function setEvent($element){
        if (is_null($element) && count($element) == 0) return false;

        $eventContainer = $this->ical->newComponent("vevent");
        
        //UID
        $eventContainer->setProperty("uid", $element["PL"] . "+planning-ema@gmx.com");
        
        //Modified date
        $modifiedUTC = gmdate('Ymd', strtotime($element["DATEMODIF"])) . 'T' . gmdate("His", 0) . 'Z';
        $eventContainer->setProperty("dtstamp", $modifiedUTC);
        $eventContainer->setProperty("created", $modifiedUTC);
        $eventContainer->setProperty("last-modified", $modifiedUTC);
        
        //Event date
        $eventContainer->setProperty("dtstart", [
            "year"  => date("Y", strtotime($element['DATE'])),
            "month" => date("m", strtotime($element['DATE'])),
            "day"   => date("d", strtotime($element['DATE'])),
            "hour"  => date("H", strtotime($element['HD'])),
            "min"   => date("i", strtotime($element['HD'])),
            "sec"   =>    0]);
        $eventContainer->setProperty("dtend", [
            "year"  => date("Y", strtotime($element['DATE'])),
            "month" => date("m", strtotime($element['DATE'])),
            "day"   => date("d", strtotime($element['DATE'])),
            "hour"  => date("H", strtotime($element['HF'])),
            "min"   => date("i", strtotime($element['HF'])),
            "sec"   =>    0]);
            
        //Content
        $eventContainer->setProperty("summary", 
            ((!empty($element['TYPE']) && $element['TYPE'] != "-") ? $element['TYPE'] . ' ' : '') .
            $element['COURS']
        ); 
        $eventContainer->setProperty("description", 
            $element['COURS'] .
            "\nEnseignant " . $element['PROF'] .
            ((!empty($element['GROUPE']) && $element['GROUPE'] != "-") ? "\nGroupe " . $element['GROUPE'] : '') .
            ((!empty($element['SALLE']) && $element['SALLE'] != "-") ? "\nSalle " . $element['SALLE'] : '') .
            ((!empty($element['LANOTE']) && $element['LANOTE'] != "-") ? "\n(" . $element['LANOTE'] . ')' : '')
        );
        
        if (!empty($element['SALLE']) && $element['SALLE'] != "-")
            $eventContainer->setProperty("location", "Salle " . $element['SALLE']);
    }
}