<?php

namespace app\classes;


class SelectorEleve implements iSelector
{
    protected static $key = "evcleunik";
    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getKey()
    {
        return self::$key;
    }
}