<?php

namespace app\classes;

class ColorReader {
    private $inputInt;
    
    public function setInt($int){
        $this->inputInt = $int;
        return $this;
    }
    
    public function getRGB(){
        return [
            "R" => floor($this->inputInt % 256),
            "G" => floor($this->inputInt % 65536 / 256),
            "B" => floor($this->inputInt / 65536)
        ];
    }
    
    public function getHex(){
        $rgb = self::getRGB();
        return sprintf("#%02x%02x%02x", $rgb['R'], $rgb['G'], $rgb['B']);
    }
}